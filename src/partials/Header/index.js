import React from 'react';
import { NavLink } from 'react-router-dom';

import './Header.scss';

const Header = () => {
    return (
        <header>
            <h3>Header</h3>
            <ul>
                <li>
                    <NavLink to="/">Home</NavLink>
                </li>
                <li>
                    <NavLink to="/about-us">About</NavLink>
                </li>
                <li>
                    <NavLink to="/blog">Blog</NavLink>
                </li>
                <li>
                    <NavLink to="/contact-us">Contact</NavLink>
                </li>
            </ul>
        </header>
    )
}

export default Header;