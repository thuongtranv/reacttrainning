import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from '../reducers';

const configureStore = (initalStage) => {
    const composeEnhances = composeWithDevTools(
        applyMiddleware(thunk)
    );

    const store = createStore(rootReducer, initalStage, composeEnhances);

    if(module.hot){
        module.hot.accept('../reducers', () => {
            store.replaceReducer(require('../reducers').default)
        })
    }

    return store;
}

export default configureStore;