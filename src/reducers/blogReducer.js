import * as constant from '../constants'

const initialStage = {
    loading: false,
    data: [],
    message: null
}

const blogReducer = (state = initialStage, action) => {
    switch(action.type){
        case constant.REQUEST_BLOG_START:
            return {
                ...state,
                loading: true,
                message: null
            }
        case constant.REQUEST_BLOG_FINISHED:
            return {
                ...state,
                loading: false,
                data: action.payload,
                message: null
            }
        case constant.REQUEST_BLOG_FAILED:
            return {
                ...state,
                loading: false,
                message: action.message
            }
        default:
            return {
                ...state
            }
    }
}

export default blogReducer;