import axios from 'axios';
import * as constants from '../constants';

const client = axios.create({
    baseURL: 'http://localhost:3000'
})

export const getBlog = () =>{
    return async (dispatch) => {
        try{
            dispatch(requestBlogAction());
            const postData = await client.get("/mockdata/blog.json");
            dispatch(requestSuccessAction(postData.data));
        }
        catch(err){
            dispatch(requestFailedAction());
        }
    }
}

const requestBlogAction = () =>{
    return {
        type: constants.REQUEST_BLOG_START
    }
}

const requestSuccessAction = (data) => {
    return {
        type: constants.REQUEST_BLOG_FINISHED,
        payload: data
    }
}

const requestFailedAction = () => {
    return {
        type: constants.REQUEST_BLOG_FAILED,
        message: "Loading data error!"
    }
}