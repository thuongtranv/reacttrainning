import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Switch, Route } from 'react-router-dom';

import Home from './screens/Home';
import Blog from './screens/Blog';
import About from './screens/About';
import Contact from './screens/Contact';
import PostContent from './screens/PostContent';
import NotFound from './screens/NotFound';
import Header from './partials/Header';
import Footer from './partials/Footer';

function App() {
  return (
    <div className="App">
      <Header/>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/about-us" component={About}/>
        <Route exact path="/contact-us" component={Contact}/>
        <Route exact path="/blog" component={Blog}/>
        <Route path="/post/:id" component={PostContent}/>
        <Route path="*" component={NotFound}/>
      </Switch>
      <Footer/>
    </div>
  );
}

export default App;
