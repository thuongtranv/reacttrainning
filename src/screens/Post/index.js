import React from 'react';
import { Link } from 'react-router-dom';

const Post = props => {
    const { item } = props;
}

return (
    <div className="post col-xl-6">
    <div className="post-thumbnail">
      <Link to={`/post/${item.id}`}>
        <img src={item.image} alt={item.title} className="img-fluid" />
      </Link>
    </div>
    <div className="post-details">
      <div className="post-meta d-flex justify-content-between">
        <div className="date meta-last">{item.createAt}</div>
        <div className="category"><Link to="#">{item.category}</Link></div>
      </div>
      <Link to={`/post/${item.id}`}>
        <h3 className="h4">{item.title}</h3>
      </Link>
      <p className="text-muted">{item.description}</p>
      <footer className="post-footer d-flex align-items-center">
        <Link to="#" className="author d-flex align-items-center flex-wrap">
          <div className="avatar">
            <img src={item.avatar} alt={item.author} className="img-fluid" />
          </div>
          <div className="title"><span>{item.author}</span></div>
        </Link>
        <div className="date"><i className="icon-clock" />{item.timeAgo}</div>
        <div className="comments meta-last"><i className="icon-comment" />{item.comment}</div>
      </footer>
    </div>
  </div>
)

export default Post;