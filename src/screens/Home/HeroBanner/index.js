import React from 'react';
import { Link } from 'react-router-dom';

import styles from './HeroBanner.module.scss';

const HeroBanner = () => {
    return (
        <section className={styles.hero}>
            <div className={styles.container}>
                <div className="row">
                    <div className="col-lg-7">
                        <h1>Bootstrap 4 Blog - A free template by Bootstrap Temple</h1>
                        <Link to="#" className={styles['hero-link']}>Discover More</Link>
                    </div>
                </div>
                <Link to=".intro" className={`${styles.continue} link-scroll`}>
                    <i className="fa fa-long-arrow-down" /> Scroll Down
                </Link>
            </div>
        </section>
    )
}

export default HeroBanner;