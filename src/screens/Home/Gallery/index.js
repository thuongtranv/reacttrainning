import React from 'react';

const Gallery = ({ item }) => {
    return (
        <div className="mix col-lg-3 col-md-3 col-sm-6">
            <div className="item">
                <a href={item.img} data-fancybox="gallery" className="image">
                    <img src={item.img} alt={item.alt} className="img-fluid" />
                    <div className="overlay d-flex align-items-center justify-content-center">
                        <i className="icon-search" />
                    </div>
                </a>
            </div>
        </div>
    )
}

export default Gallery;