import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import HeroBanner from './HeroBanner';
import Post from './Post';
import LatestFromBlog from './LatestFromBlog';
import NewsLetter from './Newsletter';
import Gallery from './Gallery';
import styles from './Home.module.scss';

const HomePage = ({ posts, blogLatest, gallery })=> {
  return(
      <>
          <HeroBanner />
          <section className="intro">
            <div className="container">
              <div className="row">
                <div className="col-lg-8">
                  <h2 className="h3">Some great intro here</h2>
                  <p className="text-big">Place a nice <strong>introduction</strong> here <strong>to catch reader's
                  attention</strong>. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud nisi ut aliquip ex ea
                  commodo consequat. Duis aute irure dolor in reprehenderi.</p>
                </div>
              </div>
            </div>
          </section>
          <section className="featured-posts no-padding-top">
            <div className="container">
              {posts.map((item,key) => {
                const isOdd = key%2;
                console.log(isOdd);
                return <Post item={item} key={key} imgOnLeft={isOdd}/>
              })}
            </div>
          </section>
          <section className={styles.divider}>
            <div className="container">
              <div className="row">
                <div className="col-md-7">
                  <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                  labore et dolore magna aliqua</h2>
                  <Link to="#" className="hero-link">View More</Link>
                </div>
              </div>
            </div>
          </section>
          <section className="latest-posts">
            <div className="container">
              <header>
                <h2>Latest from the blog</h2>
                <p className="text-big">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
              </header>
              <div className="row">
                {blogLatest.map((item,key) => {
                  return <LatestFromBlog item={item} key={key}/>
                })}
              </div>
            </div>
          </section>
          <NewsLetter/>
          <section className="gallery no-padding">
            <div className="row">
              {gallery.map((item, key) => {
                return <Gallery item={item} key={key}/>
              })}
            </div>
          </section>
      </>
  )
}

const postPropTypes = {
  categories: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired,
  postUrl: PropTypes.string.isRequired,
  postImg: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  datePost: PropTypes.string.isRequired,
  totalComment: PropTypes.number
};

const postLatestPropTypes = {
  title: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired,
  category: PropTypes.string.isRequired,
  datePost: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
};

const galleryPropTypes = {
  img: PropTypes.string.isRequired,
  alt: PropTypes.string
};

HomePage.propTypes = {
  posts: PropTypes.arrayOf(
    PropTypes.shape(postPropTypes)
  ).isRequired,

  blogLatest: PropTypes.arrayOf(
    PropTypes.shape(postLatestPropTypes)
  ).isRequired,

  gallery: PropTypes.arrayOf(
    PropTypes.shape(galleryPropTypes)
  ).isRequired,
};

HomePage.defaultProps = {
    posts: [
        {
          categories: [
            "business",
            "technology"
          ],
          title: "Alberto Savoia Can Teach You About Interior",
          postUrl: "post.html",
          postImg: "/assets/img/featured-pic-1.jpeg",
          description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam, quis nostrude consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt.`,
          avatar: "/assets/img/avatar-1.jpg",
          name: "John Doe",
          datePost: "2 months ago",
          totalComment: 12
        },
        {
          categories: [
            "business",
            "technology"
          ],
          title: "Alberto Savoia Can Teach You About Interior",
          postUrl: "post.html",
          postImg: "/assets/img/featured-pic-2.jpeg",
          description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam, quis nostrude consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt.`,
          avatar: "/assets/img/avatar-2.jpg",
          name: "John Doe",
          datePost: "2 months ago",
          totalComment: 12
        },
        {
          categories: [
            "business",
            "technology"
          ],
          title: "Alberto Savoia Can Teach You About Interior",
          postUrl: "post.html",
          postImg: "/assets/img/featured-pic-3.jpeg",
          description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                  dolore magna aliqua. Ut enim ad minim veniam, quis nostrude consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt.`,
          avatar: "/assets/img/avatar-3.jpg",
          name: "John Doe",
          datePost: "2 months ago",
          totalComment: 12
        }
      ],
      blogLatest: [
        {
          title: "Ways to remember your important ideas",
          img: "/assets/img/blog-1.jpg",
          category: "Business",
          datePost: "20 May | 2016",
          description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
              incididunt ut labore.`
        },
        {
          title: "Ways to remember your important ideas",
          img: "/assets/img/blog-2.jpg",
          category: "Business",
          datePost: "20 May | 2016",
          description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
              incididunt ut labore.`
        },
        {
          title: "Ways to remember your important ideas",
          img: "/assets/img/blog-3.jpg",
          category: "Business",
          datePost: "20 May | 2016",
          description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
              incididunt ut labore.`
        }
      ],
      gallery: [
        {
          img: "/assets/img/gallery-1.jpg",
          alt: "Image 1"
        },
        {
          img: "/assets/img/gallery-2.jpg",
          alt: "Image 2"
        },
        {
          img: "/assets/img/gallery-3.jpg",
          alt: "Image 3"
        },
        {
          img: "/assets/img/gallery-4.jpg",
          alt: "Image 4"
        }
      ]    
};

export default HomePage;