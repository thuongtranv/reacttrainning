import React from 'react';
import { Link } from "react-router-dom";

const LatestFromBlog = ({ item }) => {
    return (
        <div className="post col-md-4">
            <div className="post-thumbnail">
                <Link to="post.html">
                    <img src={item.img} alt="..." className="img-fluid"/>
                </Link>
            </div>
            <div className="post-details">
                <div className="post-meta d-flex justify-content-between">
                    <div className="date">18 Jun | 2020</div>
                    <div className="category">
                        <Link to="#">Business</Link>
                    </div>
                </div>
                <Link to="post.html">
                    <h3 className="h4">Ways to remember your important ideas</h3>
                </Link>
                <p className="text-muted">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                    incididunt ut labore.
                </p>
            </div>
        </div>
    )
};

export default LatestFromBlog;