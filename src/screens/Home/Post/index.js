import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const Post = ({ item, imgOnLeft }) => {
    return (
        <div className="row d-flex align-items-stretch">
            {imgOnLeft !== 0 && (
                <div className="image col-lg-5">
                    <img src={item.postImg} alt={item.title}/>
                </div>
            )}
            <div className="text col-lg-7">
                <div className="text-inner d-flex align-items-center">
                    <div className="content">
                        <header className="post-header">
                            <div className="category">
                                {item.categories.length > 0 && item.categories.map((category, key) => {
                                    return <Link to="#" key={key}>{category}</Link>
                                })}
                            </div>
                            <Link to="post.html">
                                <h2 className="h4">{item.title}</h2>
                            </Link>
                        </header>
                        <p>{item.description}</p>
                        <footer className="post-footer d-flex align-items-center">
                            <Link to="#" className="author d-flex align-items-center flex-wrap">
                                <div className="avatar">
                                    <img src={item.avatar} alt={item.name} className="img-fluid"/>
                                </div>
                                <div className="title"><span>{item.name}</span></div>
                            </Link>
                            <div className="date"><i className="icon-clock" />{item.datePost}</div>
                            <div className="comments"><i className="icon-comment" />{item.totalComment}</div>
                        </footer>
                    </div>
                </div>
            </div>
            {imgOnLeft === 0 && (
                <div className="image col-lg-5">
                    <img src={item.postImg} alt={item.title}/>
                </div>
            )}
        </div>
    )
};

const itemPropTypes = {
    categories: PropTypes.array.isRequired,
    title: PropTypes.string.isRequired,
    postUrl: PropTypes.string.isRequired,
    postImg: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    avatar: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    datePost: PropTypes.string.isRequired,
    totalComment: PropTypes.number
};

Post.propTypes = {
    item: PropTypes.shape(itemPropTypes).isRequired,
    imgOnLeft: PropTypes.number.isRequired
}

export default Post;