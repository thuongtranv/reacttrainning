import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getBlog } from "../../actions";
import Post from '../Post';
import './Blog.scss';

const Blog = () =>{
    const { blog } = useSelector(state => {
      return {
        blog: state.blog
      }
    })

    const dispatch = useDispatch();

    useEffect(() => {
      dispatch(getBlog());
    }, [dispatch]);

    const loadingIcon = (
      <div className="spinner-border text-primary" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    )

    const listItems = blog.data.length > 0 && blog.data.map((item, index) => {
      return <Post key={index} item={item} />
    })

    const content = blog.message ? blog.message : listItems;

    return (
      <div className="container">
        <div className="row">
          <main className="posts-listing col-lg-8">
            <div className="container">
              <div className="row">
                {blog.loading && loadingIcon}
                {!blog.loading && content}
              </div>
            </div>
          </main>
          
        </div>
      </div>
    )
}

export default Blog;

